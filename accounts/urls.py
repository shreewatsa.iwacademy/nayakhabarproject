
from django.urls import path
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views

from . import views


app_name = "accounts"
urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name="accounts/login.html"), name="login"),
    # path('login/', views.LoginView.as_view(), name="login"),
    # path('logout/', TemplateView.as_view(template_name="accounts/login.html"), name="logout"),
    path('logout/', auth_views.LogoutView.as_view(), name="logout"),
    path('register/', TemplateView.as_view(template_name="accounts/login.html"), name="register"),
]