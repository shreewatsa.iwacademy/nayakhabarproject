from django.db import models
from django.shortcuts import reverse

# from accounts.models import User

# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    publisher = models.ForeignKey("accounts.User", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    is_published = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey("accounts.User",related_name="updater", on_delete=models.CASCADE, null=True, blank=True)

    def get_absolute_url(self):
        return reverse("news:article_details", kwargs={"pk": self.pk})
    
    def __str__(self):
        return self.title[:20]
    

class ArticleImages(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='articles/')
    caption = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.article.title[:20]
    